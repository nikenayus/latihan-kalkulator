// untuk menampilkan hasil dari kalkulator
var tampil = document.formAngka.isiText;

//untuk mengidentifikasikan angka dan operator
function ins(num){
    tampil.value += num;
}

// untuk menghapus semua angka 
function hapusAll(){
    tampil.value = "";
}

// untuk mengahapus angka tetapi tidak terhapus semuanya hanya satu angka yang kehapus
function bcksp(){
    tampil.value = tampil.value.substr(0,tampil.value.length-1);
}

// untuk menampilkan hasil perhitungan yang kita hitung 
function samaDengan(){
    tampil.value = eval(tampil.value); 
}